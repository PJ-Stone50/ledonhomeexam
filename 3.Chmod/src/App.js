const Chmod = (props) => {
  const { permission } = props;

  function convert(input) {
    const conversionTable = {
      '-': 0,
      'r': 4,
      'w': 2,
      'x': 1
    };

    let resultGroup = '';
    let groups;

    if (input.length >= 9 && input.length < 10) {
      groups = input.match(/(.{1,3})(.{1,3})(.{1,3})/).slice(1); // Split input into groups of 4-3-3 characters
      console.log(9,groups)
    } else {
      groups = input.match(/(.{1,4})(.{1,3})(.{1,3})/).slice(1); // Split input into groups of 3-3-3 characters
      console.log(10,groups)
    }

    for (let group of groups) {
      let numericGroup = 0;
      for (let char of group) {
        numericGroup += conversionTable[char];
      }
      resultGroup += numericGroup;
    }

    return parseInt(resultGroup, 10);
  }

  return (
    <>
      <p>
        {`[ ${permission} ] =  `}
        {convert(permission)}
      </p>
    </>
  );
};

export default function App() {
  return (
    <div className="App">
      <Chmod permission={"rwxrwxr-x"} /> {/*775*/}
      <Chmod permission={"-r-w------"} /> {/*600*/}
      <Chmod permission={"r-wr----r"} /> {/*644*/}
      <Chmod permission={"-rwxrwxr-x"} /> {/*775*/}
      <Chmod permission={"-r-x---r-x"} /> {/*505*/}
      <Chmod permission={"-rw-rw-rw-"} /> {/*666*/}
      <Chmod permission={"-r--r--r--"} /> {/*444*/}
      <Chmod permission={"---x--x--x"} /> {/*111*/}
      <Chmod permission={"-rw-------"} /> {/*600*/}
      <Chmod permission={"-rw-rw-r--"} /> {/*644*/}
    </div>
  );
}
