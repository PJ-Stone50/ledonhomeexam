const express = require('express');
const app = express();
const port = 3001;
const fs = require('fs').promises;
const path = require('path');

class Navigations {
    constructor(id, title, translate, children, icon, type) {
      this.id = id;
      this.title = title;
      this.translate = translate;
      this.children = children;
      this.icon = icon;
      this.type = type;

    }

}



app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.get('/getNavigations', async (req, res) => {
    try {
        const filePath = path.join(__dirname, 'test.navigations.json');
        const data = await fs.readFile(filePath, 'utf8');
        const jsonData = JSON.parse(data);

        const navigationArray = []
        
        
        
        jsonData.forEach(element => {
            const navigations = new Navigations() 
            navigations.id = element.key
            navigations.title = element.comment
            navigations.translate = element.name
            navigations.icon = element.icon
            navigations.type = element.type


            navigationArray.push(navigations)
            // console.log(navigations)


            // if(element.navigation_id ) {
            //     jsonData.forEach(element => {
            //         navigations.children = element
            //         // console.log(navigations.children)
                    
            //     });
            // }
 
            // jsonData.forEach(element => {
                
            //     if(element.navigation_id && element.navigation_id == navigations.id) {
            //         // Correct children
            //         console.log("FOUND")
            //     }
            // });
        });
        

        navigationArray.forEach(element => {
            if(element.navigation_id) {
                console.log(element)
                navigationArray.forEach(element => {
                    console.log(element)
                });
            }
        });
        
        res.send(navigationArray)

    } catch (error) {
        console.error('Error reading or parsing JSON:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
